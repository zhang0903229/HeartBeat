package com.andaily.hb.domain.application;

import com.andaily.hb.infrastructure.DateUtils;
import org.junit.Test;
import org.quartz.CronExpression;


import java.util.Date;

import static org.junit.Assert.*;


/**
 * @author Shengzhao Li
 */
public class HeartBeatFrequencyTest {


    /*
    * 测试各类 cron expression
    * */
    @Test
    public void cron() throws Exception {
        Date date = DateUtils.getDate("2016-12-12 12:00:00", DateUtils.DEFAULT_DATE_TIME_FORMAT);

        //5秒
        String cron = "0/5 * * * * ?";
        CronExpression cronExpression = new CronExpression(cron);
        Date next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:00:05");

        //10秒
        cron = "0/10 * * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:00:10");

        //20秒
        cron = "0/20 * * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:00:20");

        //30秒
        cron = "0/30 * * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:00:30");

        //1分钟
        cron = "0 0/1 * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:01:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:02:00");

        //2分钟
        cron = "0 0/2 * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:02:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:04:00");

        //3分钟
        cron = "0 0/3 * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:03:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:06:00");

        //5分钟
        cron = "0 0/5 * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:05:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:10:00");

        //10分钟
        cron = "0 0/10 * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:10:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:20:00");

        //30分钟
        cron = "0 0/30 * * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 12:30:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 13:00:00");

        //1小时
        cron = "0 0 0/1 * * ?";
        cronExpression = new CronExpression(cron);
        next = cronExpression.getNextValidTimeAfter(date);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 13:00:00");

        next = cronExpression.getNextValidTimeAfter(next);
        assertNotNull(next);
        assertEquals(DateUtils.toDateText(next, DateUtils.DEFAULT_DATE_TIME_FORMAT), "2016-12-12 14:00:00");


    }


}