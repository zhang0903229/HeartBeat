package com.andaily.hb.infrastructure;


import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * @author Shengzhao Li
 */
public class PasswordHandlerTest {

    @Test
    public void testEncryptPassword() throws Exception {
        final String encryptPass = PasswordHandler.encryptPassword("admin");
        assertNotNull(encryptPass);
        System.out.println(encryptPass);

    }
}