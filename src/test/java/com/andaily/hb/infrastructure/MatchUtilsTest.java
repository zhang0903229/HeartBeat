package com.andaily.hb.infrastructure;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Shengzhao Li
 */
public class MatchUtilsTest {


    @Test
    public void isEmail() {

        boolean result = MatchUtils.isEmail("addd");
        assertFalse(result);

        result = MatchUtils.isEmail("addd@honyee.cc");
        assertTrue(result);
    }

}