package com.andaily.hb.domain.dto;

import com.andaily.hb.domain.application.ApplicationInstance;

/**
 * @author Shengzhao Li
 */
public class MonitoringInstanceDto extends IndexInstanceDto {


    public MonitoringInstanceDto(ApplicationInstance instance) {
        super(instance);
    }


}