package com.andaily.hb.domain.dto.user;

import com.andaily.hb.domain.dto.AbstractDto;
import com.andaily.hb.domain.user.SystemSetting;

/**
 * 15-4-14
 *
 * @author Shengzhao Li
 */
public class SystemSettingDto extends AbstractDto {

    private static final long serialVersionUID = 2289700406929530627L;


    private boolean allowUserRegister;

    private int cleanMonitorLogFrequency;

    private long monitorLogAmount;

    /**
     * 是否启用 代理, 默认 false
     *
     * @since 2.0.1
     */
    private boolean proxyEnabled = false;

    /**
     * 代理 host
     *
     * @since 2.0.1
     */
    private String proxyHost;

    /**
     * 代理 port
     *
     * @since 2.0.1
     */
    private int proxyPort;

    public SystemSettingDto() {
    }

    public SystemSettingDto(SystemSetting setting) {
        super(setting.guid());
        this.allowUserRegister = setting.allowUserRegister();
        this.cleanMonitorLogFrequency = setting.cleanMonitorLogFrequency();

        this.proxyEnabled = setting.proxyEnabled();
        this.proxyHost = setting.proxyHost();
        this.proxyPort = setting.proxyPort();
    }


    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    public void setProxyEnabled(boolean proxyEnabled) {
        this.proxyEnabled = proxyEnabled;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public int getCleanMonitorLogFrequency() {
        return cleanMonitorLogFrequency;
    }

    public void setCleanMonitorLogFrequency(int cleanMonitorLogFrequency) {
        this.cleanMonitorLogFrequency = cleanMonitorLogFrequency;
    }

    public boolean isAllowUserRegister() {
        return allowUserRegister;
    }

    public void setAllowUserRegister(boolean allowUserRegister) {
        this.allowUserRegister = allowUserRegister;
    }

    public long getMonitorLogAmount() {
        return monitorLogAmount;
    }

    public void setMonitorLogAmount(long monitorLogAmount) {
        this.monitorLogAmount = monitorLogAmount;
    }
}
