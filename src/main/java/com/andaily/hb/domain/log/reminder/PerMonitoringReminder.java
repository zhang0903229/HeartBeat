package com.andaily.hb.domain.log.reminder;

import com.andaily.hb.domain.log.FrequencyMonitorLog;
import com.andaily.hb.domain.log.LogRepository;
import com.andaily.hb.domain.log.MonitoringReminderLog;
import com.andaily.hb.domain.shared.BeanProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 每一次监控的提醒 操作
 *
 * @author Shengzhao Li
 */
public class PerMonitoringReminder extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerMonitoringReminder.class);

    private transient LogRepository logRepository = BeanProvider.getBean(LogRepository.class);
    private final FrequencyMonitorLog monitorLog;

    public PerMonitoringReminder(FrequencyMonitorLog monitorLog) {
        this.monitorLog = monitorLog;
    }

    /**
     * 可扩展, 若需要新线程来处理提醒操作.
     * 可使用 this.start() 启用一新线程来完成.
     */
    public void remind() {
        this.run();
    }

    @Override
    public void run() {

        if (!isNeedReminder()) {
            LOGGER.debug("Ignore Monitoring Reminder of FrequencyMonitorLog[{}]", monitorLog);
            return;
        }

        MonitoringReminderLog reminderLog = sendAndGenerateReminderLog();

        logRepository.saveOrUpdate(reminderLog);
        LOGGER.debug("Save MonitoringReminderLog[{}]", reminderLog);
    }

    private MonitoringReminderLog sendAndGenerateReminderLog() {
        PerMonitoringReminderSenderResolver senderResolver = new PerMonitoringReminderSenderResolver(monitorLog);
        List<PerMonitoringReminderSender> senders = senderResolver.resolve();
        LOGGER.debug("Resolver [{}] PerMonitoringReminderSenders as follow: {}", senders.size(), senders);

        MonitoringReminderLog reminderLog = new MonitoringReminderLog(monitorLog);
        for (PerMonitoringReminderSender sender : senders) {
            sender.send(reminderLog, monitorLog);
        }
        return reminderLog;
    }


    /**
     * 判断是否需要进行提醒
     *
     * @return True is need reminder
     */
    private boolean isNeedReminder() {
        PerMonitoringReminderChecker reminderChecker = new PerMonitoringReminderChecker(monitorLog);
        return reminderChecker.isNeedReminder();
    }
}