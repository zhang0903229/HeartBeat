package com.andaily.hb.service.impl;

import com.andaily.hb.domain.dto.user.*;
import com.andaily.hb.domain.shared.paginated.PaginatedLoader;
import com.andaily.hb.domain.shared.security.AndailyUserDetails;
import com.andaily.hb.domain.shared.security.SecurityUtils;
import com.andaily.hb.domain.user.User;
import com.andaily.hb.domain.user.UserRepository;
import com.andaily.hb.domain.user.WeixinUser;
import com.andaily.hb.infrastructure.MatchUtils;
import com.andaily.hb.service.UserService;
import com.andaily.hb.service.operation.user.RegisterUserHandler;
import com.andaily.hb.service.operation.user.SystemSettingDtoLoader;
import com.andaily.hb.service.operation.user.SystemSettingUpdater;
import com.andaily.hb.service.operation.user.UserFormDtoPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author Shengzhao Li
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Not found any user for username[" + username + "]");
        }
        return new AndailyUserDetails(user);
    }


    @Transactional(readOnly = true)
    @Override
    public UserListDto loadUserListDto(UserListDto listDto) {
        final Map<String, Object> map = listDto.queryMap();
        return listDto.load(new PaginatedLoader<UserDto>() {
            @Override
            public List<UserDto> loadList() {
                List<User> users = userRepository.findListUsers(map);
                return UserDto.toDtos(users);
            }

            @Override
            public int loadTotalSize() {
                return userRepository.totalListUsers(map);
            }
        });
    }


    @Transactional(readOnly = true)
    @Override
    public UserFormDto loadUserFormDto(String guid) {
        if (MatchUtils.isCreate(guid)) {
            return new UserFormDto();
        } else {
            return new UserFormDto(userRepository.findByGuid(guid, User.class));
        }
    }


    @Transactional(readOnly = true)
    @Override
    public boolean isExistUsername(String username) {
        User user = userRepository.findByUsernameIgnoreArchived(username);
        return user != null;
    }


    @Transactional
    @Override
    public void persistUserFormDto(UserFormDto formDto) {
        UserFormDtoPersister persister = new UserFormDtoPersister(formDto);
        persister.persist();
    }


    @Transactional
    @Override
    public void deleteUser(String guid) {
        User user = userRepository.findByGuid(guid, User.class);
        user.deleteMe();
    }


    @Transactional
    @Override
    public ResetUserPasswordDto resetUserPassword(String guid) {
        User user = userRepository.findByGuid(guid, User.class);
        final String newPassword = user.resetPassword();
        return new ResetUserPasswordDto(user, newPassword);
    }

    @Transactional
    @Override
    public void updateUserProfile(UserProfileDto profileDto) {
        User user = userRepository.findByGuid(SecurityUtils.currentUser().guid(), User.class);
        user.updatePassword(profileDto.getPassword());
    }


    @Transactional(readOnly = true)
    @Override
    public SystemSettingDto loadSystemSettingDto() {
        SystemSettingDtoLoader systemSettingDtoLoader = new SystemSettingDtoLoader();
        return systemSettingDtoLoader.load();
    }

    @Transactional
    @Override
    public void updateSystemSetting(SystemSettingDto settingDto) {
        SystemSettingUpdater updater = new SystemSettingUpdater(settingDto);
        updater.update();
    }

    @Transactional
    @Override
    public void registerUser(UserRegisterDto formDto) {
        RegisterUserHandler registerUserHandler = new RegisterUserHandler(formDto);
        registerUserHandler.handle();
    }


    @Transactional(readOnly = true)
    @Override
    public WeixinUserPaginated loadWeixinUserPaginated(WeixinUserPaginated listDto) {
        final Map<String, Object> map = listDto.queryMap();
        return listDto.load(new PaginatedLoader<WeixinUserDto>() {
            @Override
            public List<WeixinUserDto> loadList() {
                List<WeixinUser> list = userRepository.findWeixinUserList(map);
                return WeixinUserDto.toDtos(list);
            }

            @Override
            public int loadTotalSize() {
                return userRepository.totalWeixinUserList(map);
            }
        });
    }
}