package com.andaily.hb.service.operation.instance;

import com.andaily.hb.domain.application.ApplicationInstance;
import com.andaily.hb.domain.application.ApplicationInstanceRepository;
import com.andaily.hb.domain.dto.application.ApplicationInstanceDto;
import com.andaily.hb.domain.dto.application.ApplicationInstanceListDto;
import com.andaily.hb.domain.shared.BeanProvider;
import com.andaily.hb.domain.shared.paginated.PaginatedLoader;

import java.util.List;
import java.util.Map;

/**
 * 2015/9/22
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceListDtoLoader {

    private transient ApplicationInstanceRepository instanceRepository = BeanProvider.getBean(ApplicationInstanceRepository.class);

    private ApplicationInstanceListDto listDto;

    public ApplicationInstanceListDtoLoader(ApplicationInstanceListDto listDto) {
        this.listDto = listDto;
    }

    public ApplicationInstanceListDto load() {

        final Map<String, Object> map = listDto.queryMap();

        listDto.load(new PaginatedLoader<ApplicationInstanceDto>() {
            @Override
            public List<ApplicationInstanceDto> loadList() {
                List<ApplicationInstance> instances = instanceRepository.findApplicationInstanceList(map);
                return ApplicationInstanceDto.toDtos(instances);
            }

            @Override
            public int loadTotalSize() {
                return instanceRepository.totalApplicationInstanceList(map);
            }
        });

        return listDto;
    }
}
