package com.andaily.hb.service.operation.job;

import com.andaily.hb.domain.application.ApplicationInstance;
import com.andaily.hb.domain.application.InstanceMonitorURLParameter;
import com.andaily.hb.domain.application.MonitorUrlRequestMethod;
import com.andaily.hb.domain.log.FrequencyMonitorLog;
import com.andaily.hb.infrastructure.HttpClientDeleteHandler;
import com.andaily.hb.infrastructure.HttpClientHandler;
import com.andaily.hb.infrastructure.HttpClientPostHandler;
import com.andaily.hb.infrastructure.HttpClientPutHandler;
import org.apache.commons.lang.StringUtils;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.andaily.hb.domain.shared.Application.ENCODING;

/**
 * 15-3-27
 * <p/>
 * 每一次 监控的 监控日志生成操作类
 *
 * @author Shengzhao Li
 */
public class FrequencyMonitorLogGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(FrequencyMonitorLogGenerator.class);
    private ApplicationInstance instance;

    public FrequencyMonitorLogGenerator(ApplicationInstance instance) {
        this.instance = instance;
    }

    /**
     * 生成监控日志.
     * 先通过HttpClient 发送请求 并根据响应情况 记录日志
     *
     * @return FrequencyMonitorLog instance
     */
    public FrequencyMonitorLog generate() {
        HttpClientHandler httpClientHandler = createHttpClientHandler();
        LOGGER.debug("Send Request to URL: {} use HttpClientHandler: {}", monitorUrl(), httpClientHandler);

        final FrequencyMonitorLog monitorLog = httpClientHandler.handleAndGenerateFrequencyMonitorLog();
        monitorLog.instance(instance);
        return monitorLog;
    }

    /**
     * 创建 HttpClientHandler, 分GET, POST两类 请求
     *
     * @return HttpClientHandler
     */
    private HttpClientHandler createHttpClientHandler() {
        String monitorUrl = monitorUrl();
        HttpClientHandler clientHandler = createHttpClientHandler(monitorUrl);

        final List<InstanceMonitorURLParameter> urlParameters = instance.instanceURL().urlParameters();
        for (InstanceMonitorURLParameter param : urlParameters) {
            clientHandler.addRequestParam(param.key(), param.realValue());
        }

        //增加 requestBody,
        final String requestBody = instance.requestBody();
        if (StringUtils.isNotBlank(requestBody)) {
            clientHandler.httpEntity(new StringEntity(requestBody, ENCODING));
            LOGGER.debug("Request add requestBody: {}", requestBody);
        }

        return clientHandler.maxConnectionSeconds(maxConnectionSeconds())
                .useProxy(instance.useProxy())
                .contentType(instance.instanceURL().contentType());
    }

    /**
     * 增加 PUT, DELETE 的支持
     *
     * @param monitorUrl url
     * @return HttpClientHandler
     * @since 2.0.1
     */
    private HttpClientHandler createHttpClientHandler(String monitorUrl) {
        MonitorUrlRequestMethod requestMethod = instance.requestMethod();
        switch (requestMethod) {
            case PUT:
                return new HttpClientPutHandler(monitorUrl);
            case POST:
                return new HttpClientPostHandler(monitorUrl);
            case DELETE:
                return new HttpClientDeleteHandler(monitorUrl);
            default:
                //默认 GET
                return new HttpClientHandler(monitorUrl);
        }
    }

    private int maxConnectionSeconds() {
        return instance.maxConnectionSeconds();
    }

    private String monitorUrl() {
        return instance.monitorUrl();
    }
}
