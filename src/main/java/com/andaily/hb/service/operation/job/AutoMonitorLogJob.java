package com.andaily.hb.service.operation.job;

import com.andaily.hb.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * 2017/1/21
 * <p/>
 * <p/>
 * 自动清理 监控日志 的定时任务
 *
 * @author Shengzhao Li
 */
@Component
public class AutoMonitorLogJob implements InitializingBean {

    private static final Logger LOG = LoggerFactory.getLogger(AutoMonitorLogJob.class);

    @Autowired
    private transient LogService logService;


    public AutoMonitorLogJob() {
    }


    @Scheduled(cron = "${auto.clean.monitor.log.cron.expression}")
    public void execute() {
        LOG.debug("*****  Start execute Job [{}]", getClass());

        logService.executeAutoCleanMonitorLogs();

        LOG.debug("&&&&&  End execute Job [{}]", getClass());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(logService, "logService is null");
    }

}
