package com.andaily.hb.service.operation.search;

import com.andaily.hb.domain.dto.HBSearchDto;
import com.andaily.hb.domain.dto.HBSearchResultDto;
import com.andaily.hb.domain.log.FrequencyMonitorLog;
import com.andaily.hb.domain.log.LogRepository;
import com.andaily.hb.domain.shared.BeanProvider;
import com.andaily.hb.domain.shared.paginated.PaginatedLoader;

import java.util.List;
import java.util.Map;

/**
 * 15-3-13
 * <p/>
 * 监控日志的 搜索业务
 *
 * @author Shengzhao Li
 */
public class MonitorLogSearcher implements HBSearcher {

    private transient LogRepository logRepository = BeanProvider.getBean(LogRepository.class);

    public MonitorLogSearcher() {
    }

    @Override
    public HBSearchDto search(HBSearchDto searchDto) {
        final Map<String, Object> map = searchDto.queryMap();
        return searchDto.load(new PaginatedLoader<HBSearchResultDto>() {
            @Override
            public List<HBSearchResultDto> loadList() {
                List<FrequencyMonitorLog> monitorLogs = logRepository.findHBSearchMonitorLogs(map);
                return HBSearchResultDto.toMonitorLogDtos(monitorLogs);
            }

            @Override
            public int loadTotalSize() {
                return logRepository.totalHBSearchMonitorLogs(map);
            }
        });
    }
}
