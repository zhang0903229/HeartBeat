package com.andaily.hb.configuration;

import com.andaily.hb.domain.shared.security.SecurityUtils;
import com.andaily.hb.service.UserService;
import com.andaily.hb.web.context.SpringSecurityHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * 2018/2/1
 * <p>
 * Security
 * <p>
 * replace security.xml
 *
 * @author Shengzhao Li
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserService userService;



    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;



    @Override
    public void configure(WebSecurity web) throws Exception {
        //Ignore, public
        web.ignoring().antMatchers("/public/**", "/static/**");
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable()
        http.authorizeRequests()
                .antMatchers("/public/**").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/login.hb*").permitAll()

                .antMatchers("/instance/instance_form.hb*").hasAnyRole("CREATE_EDIT_INSTANCE")
                .antMatchers("/instance/delete.hb*").hasAnyRole("DELETE_INSTANCE")

                .antMatchers("/instance/enable.hb*").hasAnyRole("START_STOP_INSTANCE")
                .antMatchers("/instance/stop.hb*").hasAnyRole("START_STOP_INSTANCE")

                .antMatchers("/user/**").hasAnyRole("USER_MANAGEMENT")
                .antMatchers("/system/setting.hb*").hasAnyRole("SYSTEM_SETTING")

                .antMatchers("/user_profile.hb*").hasAnyRole("DEFAULT")

                .antMatchers(HttpMethod.GET, "/login.hb*").anonymous()
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/login.hb")
                .loginProcessingUrl("/login")
                .failureUrl("/login.hb?error=1")
                .successHandler(authenticationSuccessHandler)
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/index.hb")
                .permitAll()
                .and()
                .exceptionHandling();

        http.authenticationProvider(authenticationProvider());
    }


    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return daoAuthenticationProvider;
    }


    @Bean
    public Md5PasswordEncoder passwordEncoder() {
        return new Md5PasswordEncoder();
    }


    /**
     * SecurityUtils
     */
    @Bean
    public SecurityUtils securityUtils() {
        SecurityUtils securityUtils = new SecurityUtils();
        securityUtils.setSecurityHolder(new SpringSecurityHolder());
        return securityUtils;
    }


}
