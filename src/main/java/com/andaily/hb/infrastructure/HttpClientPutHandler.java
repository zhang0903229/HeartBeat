package com.andaily.hb.infrastructure;

import org.apache.http.client.methods.RequestBuilder;

/**
 * 2020/3/17
 * <p>
 * PUT
 *
 * @author Shengzhao Li
 * @since 2.0.1
 */
public class HttpClientPutHandler extends HttpClientHandler {
    public HttpClientPutHandler(String url) {
        super(url);
    }


    protected RequestBuilder createRequestBuilder() {
        return RequestBuilder.put();
    }
}
