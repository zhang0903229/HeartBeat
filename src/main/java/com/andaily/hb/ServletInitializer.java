package com.andaily.hb;

import com.andaily.hb.domain.shared.Application;
import com.andaily.hb.infrastructure.DateUtils;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;


/**
 * Servlet 0配置 支持
 *
 * @author Shengzhao Li
 */
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HeartBeatApplication.class);
    }


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);
        initialContextParams(servletContext);
    }


    /**
     * 向 ServletContext 中添加全局 系统变量
     */
    private void initialContextParams(ServletContext servletContext) {
        servletContext.setAttribute("startupDate", DateUtils.now());
        servletContext.setAttribute("projectHome", Application.PROJECT_HOME);
        servletContext.setAttribute("currentVersion", Application.CURRENT_VERSION);

        servletContext.log("HeartBeat context initialized, Version: " + Application.CURRENT_VERSION);
    }

}
