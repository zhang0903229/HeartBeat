package com.andaily.hb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * Spring Boot 启动引导
 *
 * @author Shengzhao Li
 */
@SpringBootApplication
@EnableScheduling
//@PropertySource(value = "classpath:HeartBeat.properties")
public class HeartBeatApplication {


    /**
     * 提示: 不能直接 运行此 main
     *
     * @param args args
     */
    public static void main(String[] args) {
        SpringApplication.run(HeartBeatApplication.class, args);
    }
}
